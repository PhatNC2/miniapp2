import {SafeAreaView, StyleSheet, Text} from 'react-native';
import React from 'react';

const App = () => {
  return (
    <SafeAreaView>
      <Text>App 2</Text>
    </SafeAreaView>
  );
};

export default App;

const styles = StyleSheet.create({});
